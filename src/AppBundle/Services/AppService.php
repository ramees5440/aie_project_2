<?php

namespace AppBundle\Services;

class AppService {

    public function __construct($em, $token, $container) {
        $this->em = $em;
        $this->token = $token;
        $this->container = $container;
    }

    public function getDates() {

        $dates = [];
        $today = new \DateTime();
        $dates['today'] = $today;
        return $dates;
    }

    public function getTomorrow() {

        $dates = [];
        $tomorrow = new \DateTime('tomorrow');
        $dates['tomorrow'] = $tomorrow;
        return $dates;
    }

    public function generateOTP() {

        $numeric = str_shuffle('0123456789');
        $subString = substr($numeric, 0, 6);
        $code = str_shuffle($subString);
        return $code;
    }

    public function timeDiffInSecond($createdDateTime = '') {
        $currentTime = new \DateTime();
        $difference = $createdDateTime->diff($currentTime);
        $hr = $difference->h;
        $minute = $difference->i;
        $second = $difference->s;
        $inSecond = ($hr * 3600) + ($minute * 60) + ($second);
        return $inSecond;
    }

    public function getEmployeeList() {

        $user = $this->token->getToken()->getUser();
        $adminProfile = $this->em->getRepository('AppBundle:Employee')->getEmployeeList();
        return $adminProfile;
    }

}
