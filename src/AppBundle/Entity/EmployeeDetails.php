<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmployeeDetails
 *
 * @ORM\Table(name="employee_details")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeDetailsRepository")
 */
class EmployeeDetails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="integer")
     */
    private $age;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_joining", type="datetime")
     */
    private $dateOfJoining;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EmployeeDetails
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return EmployeeDetails
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set dateOfJoining
     *
     * @param \DateTime $dateOfJoining
     *
     * @return EmployeeDetails
     */
    public function setDateOfJoining($dateOfJoining)
    {
        $this->dateOfJoining = $dateOfJoining;

        return $this;
    }

    /**
     * Get dateOfJoining
     *
     * @return \DateTime
     */
    public function getDateOfJoining()
    {
        return $this->dateOfJoining;
    }
}

