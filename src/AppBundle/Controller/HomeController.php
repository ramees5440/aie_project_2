<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $getEmployeeList = $em->getRepository('AppBundle:Employee')->getEmployeeList();
        $getEmployeeListSrvc = $this->get('app.service')->getEmployeeList();
        return $this->render('default/index.html.twig', ['employees' => $getEmployeeList]);
    }

}
