<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmployeeDetails;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Employeedetail controller.
 *
 */
class EmployeeDetailsController extends Controller
{
    /**
     * Lists all employeeDetail entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $employeeDetails = $em->getRepository('AppBundle:EmployeeDetails')->findAll();

        return $this->render('employeedetails/index.html.twig', array(
            'employeeDetails' => $employeeDetails,
        ));
    }

    /**
     * Creates a new employeeDetail entity.
     *
     */
    public function newAction(Request $request)
    {
        $employeeDetail = new EmployeeDetails();
        $form = $this->createForm('AppBundle\Form\EmployeeDetailsType', $employeeDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($employeeDetail);
            $em->flush();

            return $this->redirectToRoute('employeedetails_show', array('id' => $employeeDetail->getId()));
        }

        return $this->render('employeedetails/new.html.twig', array(
            'employeeDetail' => $employeeDetail,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a employeeDetail entity.
     *
     */
    public function showAction(EmployeeDetails $employeeDetail)
    {
        $deleteForm = $this->createDeleteForm($employeeDetail);

        return $this->render('employeedetails/show.html.twig', array(
            'employeeDetail' => $employeeDetail,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing employeeDetail entity.
     *
     */
    public function editAction(Request $request, EmployeeDetails $employeeDetail)
    {
        $deleteForm = $this->createDeleteForm($employeeDetail);
        $editForm = $this->createForm('AppBundle\Form\EmployeeDetailsType', $employeeDetail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employeedetails_edit', array('id' => $employeeDetail->getId()));
        }

        return $this->render('employeedetails/edit.html.twig', array(
            'employeeDetail' => $employeeDetail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a employeeDetail entity.
     *
     */
    public function deleteAction(Request $request, EmployeeDetails $employeeDetail)
    {
        $form = $this->createDeleteForm($employeeDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($employeeDetail);
            $em->flush();
        }

        return $this->redirectToRoute('employeedetails_index');
    }

    /**
     * Creates a form to delete a employeeDetail entity.
     *
     * @param EmployeeDetails $employeeDetail The employeeDetail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EmployeeDetails $employeeDetail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('employeedetails_delete', array('id' => $employeeDetail->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
